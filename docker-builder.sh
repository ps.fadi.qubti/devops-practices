#!/bin/bash

now="$(date +'%y%m%d')"
git_hash="$(git rev-parse HEAD)"
git_hash_sub=${git_hash:0:8}
version="$now-$git_hash_sub"
 
docker build -t fadiqubti/devops-practice:$version .
docker push fadiqubti/devops-practice:$version
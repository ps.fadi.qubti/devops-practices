FROM openjdk:11

ARG JarFile=*.jar

ENV PROFILE=h2 

COPY /target/$JarFile ./app.jar

EXPOSE 8070

ENTRYPOINT ["java", "-jar", "-Dserver.port=8070 ", "-Dspring.profiles.active=${PROFILE}", "app.jar"]
